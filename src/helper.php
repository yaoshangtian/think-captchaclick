<?php

Route::get('captchaclick/[:id]', "\\think\\captchaclick\\CaptChaClickController@index");

Validate::extend('captchaclick', function ($value, $id = '') {
    return captcha_check($value, $id);
});

Validate::setTypeMsg('captchaclick', ':attribute错误!');


/**
 * @param string $id
 * @param array $config
 * @return \think\Response
 */
function captchaclick($id = '', $config = [])
{
    $captcha = new \think\captchaclick\CaptChaClick($config);
    return $captcha->entry($id);
}

/**
 * @param string $value 参数（x1,y1-x2,y2-x3,y3...;imageW;imageH） 规格作处理
 * @param string $id
 */
function check_captchaclick($value,$id='')
{
    $captcha = new \think\captchaclick\CaptChaClick((array) Config::pull('captchaclick'));
    return $captcha->check($value, $id);
}


/**
 * @param $id
 * @return string
 */
function captchaclick_src($id = '')
{
    return Url::build('/captchaclick' . ($id ? "/{$id}" : ''));
}


/**
 * @param $id
 * @return mixed
 */
function captchaclick_img($id = '')
{
    return '<img src="' . captchaclick_src($id) . '" alt="captchaclick" />';
}

