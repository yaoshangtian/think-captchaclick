<?php
namespace think\captchaclick;

use think\facade\Config;

class CaptChaClickController
{
    public function index($id = "")
    {
        $captcha = new CaptChaClick((array) Config::pull('captchaclick'));
        return  $captcha->entry($id);
    }
}
